let iconMenu = document.querySelector('.gears');

lottie.loadAnimation({
        container: iconMenu,
        renderer: 'svg',
        loop: true,
        autoplay: true,
        path: "assets/gears.json"
});